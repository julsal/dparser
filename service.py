import flask
import nltk
import simplejson as json

import extractor

app = flask.Flask(__name__)


@app.route("/", methods=["POST"])
def extract():
    json_queries = flask.request.json

    if "names" in json_queries.keys():
        descs = []
        for name in json_queries["names"]:
            print ("processing '", name, "'...")
            core, tags = extractor._parse(name)
            desc = extractor.Descriptor(name, '', core)
            descs.append(desc)
            print ("done!")

        return json.dumps({"data": descs}, separators=(',', ': '), sort_keys=True)

    return "Malformed Json Input"

if __name__ == "__main__":
    nltk.download('maxent_treebank_pos_tagger')
    nltk.download('punkt')
    nltk.download('averaged_perceptron_tagger')
    app.run(host='0.0.0.0', port=8170, debug=True)