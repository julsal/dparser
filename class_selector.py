import re
import urllib


def get_class_uri(sentence):
    ssent = re.split(r"(<|>|\n| )", sentence)
    items = [s for s in ssent if len(s) > 1]

    if(len(items) != 3):
        print (items)
    else:
        if("http://dbpedia.org/class/yago" in items[0]):
            return items[0]

def main():
    classes = []
    with open("/Users/juliano/Downloads/yago_type_links.nt", "r") as f:
        for line in f:
            uri = get_class_uri(line)
            if(uri):
                classes.append(uri)

    with open("../new_selected_classes", "w") as f:
        for uri in classes:
            class_name = uri.split("/")[-1]
            print (class_name)
            class_name = urllib.unquote(class_name).decode('utf-8')
            f.write(class_name)
            f.write("  -:-  <" + uri + ">\n")

if __name__ == "__main__":
    main()
