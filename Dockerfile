FROM python:3.6

ARG PIP_PROXY

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN echo $PIP_PROXY

RUN pip $PIP_PROXY install --upgrade pip
RUN pip $PIP_PROXY install -r requirements.txt

COPY *.py /app/

EXPOSE 8170

ENTRYPOINT [ "python3" ]
CMD [ "service.py" ]
